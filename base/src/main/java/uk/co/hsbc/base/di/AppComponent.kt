package uk.co.hsbc.base.di

import android.app.Application
import dagger.Component
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by 44114587 on 08/06/2018.
 */
@Singleton
@Component(modules = arrayOf(AppModule::class))
interface AppComponent {
    fun inject(application: Application)

    @Named("Grettings")
    fun provideGrettings(): String
}