package uk.co.hsbc.base.di

import javax.inject.Scope

/**
 * Created by 44114587 on 08/06/2018.
 */
@Scope
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
annotation class NetworkScope