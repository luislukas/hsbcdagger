package uk.co.hsbc.base.di

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by 44114587 on 08/06/2018.
 */
@Module
class AppModule(val mContext: Context) {

    @Provides
    @Singleton
    fun provideContext(): Context {
        return mContext
    }

    @Provides
    @Singleton
    @Named("Grettings")
    fun provideGrettings(): String {
        return "Greetings from appModule"
    }

}