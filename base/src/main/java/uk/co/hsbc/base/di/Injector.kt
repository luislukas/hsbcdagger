package uk.co.hsbc.base.di

import android.app.Application
import android.content.Context

class Injector {

    companion object {
        lateinit var sAppComponent: AppComponent

        fun init(context: Context) {
            sAppComponent = DaggerAppComponent.builder().appModule(AppModule(context)).build()
            sAppComponent.inject(context as Application)
        }
    }
}