package uk.co.hsbc.logon.di

import dagger.Component
import uk.co.hsbc.base.di.ActivityScope
import uk.co.hsbc.logon.view.LogonActivity

/**
 * Created by 44114587 on 08/06/2018.
 */
@ActivityScope
@Component(dependencies = arrayOf(LogonNetworkComponent::class), modules = arrayOf(LogonModule::class))
interface LogonComponent {
    fun inject(activity: LogonActivity)
}