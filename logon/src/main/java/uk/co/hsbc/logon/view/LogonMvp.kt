package uk.co.hsbc.logon.view

/**
 * Created by 44114587 on 08/06/2018.
 */
interface LogonMvp {

    interface Presenter {
        fun attach(view: View)
        fun detach()
    }

    interface View {
        fun displayMessage(greetings: String)
    }

}