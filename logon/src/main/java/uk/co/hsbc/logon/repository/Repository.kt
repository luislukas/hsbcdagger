package uk.co.hsbc.logon.repository

/**
 * Repository class simulates a fetch in the server
 *
 */
interface Repository {
    /***
     * Fetches data from server
     * @returns String from server
     */
    fun fetch(): String
}