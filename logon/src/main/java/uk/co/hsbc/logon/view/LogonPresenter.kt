package uk.co.hsbc.logon.view

import uk.co.hsbc.logon.repository.Repository

class LogonPresenter(private val mRepository: Repository): LogonMvp.Presenter {

    private var mView: LogonMvp.View? = null

    override fun attach(view: LogonMvp.View) {
        mView = view
        mView?.displayMessage(mRepository.fetch())
    }

    override fun detach() {
        mView = null
    }

}