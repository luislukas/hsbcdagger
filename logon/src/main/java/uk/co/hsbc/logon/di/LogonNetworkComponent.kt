package uk.co.hsbc.logon.di

import dagger.Component
import uk.co.hsbc.base.di.AppComponent
import uk.co.hsbc.base.di.NetworkScope
import uk.co.hsbc.logon.repository.Repository

/**
 * Created by 44114587 on 08/06/2018.
 */
@NetworkScope
@Component(dependencies = arrayOf(AppComponent::class), modules = arrayOf(LogonNetworkModule::class))
interface LogonNetworkComponent {
    fun provideRepository(): Repository
}