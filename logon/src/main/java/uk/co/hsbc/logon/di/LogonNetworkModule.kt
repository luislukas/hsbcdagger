package uk.co.hsbc.logon.di

import dagger.Module
import dagger.Provides
import uk.co.hsbc.base.di.NetworkScope
import uk.co.hsbc.logon.repository.Repository
import uk.co.hsbc.logon.repository.RepositoryImpl
import javax.inject.Named

@Module
class LogonNetworkModule {

    @Provides
    @NetworkScope
    fun provideRepository(@Named("Grettings")grettings: String): Repository {
        return RepositoryImpl(grettings)
    }
}