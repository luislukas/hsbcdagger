package uk.co.hsbc.logon.repository

/**
 *
 * Class RepositoryImpl has injected mGreetings that comes from a singleton in may AppComponent
 * This is to simulate a global ApiService + all the OKHttp configuration that will come from main AppComponent.
 *
 */
class RepositoryImpl(private val mGreetings: String): Repository {

    override fun fetch(): String {
        return mGreetings+" I am some fetched Strings From server"
    }

}