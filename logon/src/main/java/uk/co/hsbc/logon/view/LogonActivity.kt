package uk.co.hsbc.logon.view

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.logon_main.*
import uk.co.hsbc.base.di.Injector
import uk.co.hsbc.logon.R
import uk.co.hsbc.logon.di.DaggerLogonComponent
import uk.co.hsbc.logon.di.DaggerLogonNetworkComponent
import uk.co.hsbc.logon.di.LogonModule
import javax.inject.Inject

class LogonActivity: AppCompatActivity(), LogonMvp.View {

    @Inject
    lateinit var mPresenter: LogonMvp.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.logon_main)
        DaggerLogonComponent.builder().logonNetworkComponent(DaggerLogonNetworkComponent.builder().appComponent(Injector.sAppComponent).build()).logonModule(LogonModule()).build().inject(this)
        mPresenter.attach(this)
    }

    override fun displayMessage(greetings: String) {
        logonGreet.setText(greetings)
    }
}