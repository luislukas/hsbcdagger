package uk.co.hsbc.logon.di

import dagger.Module
import dagger.Provides
import uk.co.hsbc.base.di.ActivityScope
import uk.co.hsbc.logon.repository.Repository
import uk.co.hsbc.logon.view.LogonMvp
import uk.co.hsbc.logon.view.LogonPresenter


@Module
class LogonModule {

    @Provides
    @ActivityScope
    fun provideLogonPresenter(mRepository: Repository): LogonMvp.Presenter {
        return LogonPresenter(mRepository)
    }

}