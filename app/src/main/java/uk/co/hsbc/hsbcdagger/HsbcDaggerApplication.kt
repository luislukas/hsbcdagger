package uk.co.hsbc.hsbcdagger

import android.app.Application
import uk.co.hsbc.base.di.Injector

class HsbcDaggerApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        Injector.init(this)
    }
}